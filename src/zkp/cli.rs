use clap::Parser;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Option<Command>,
}

#[derive(clap::Subcommand)]
pub enum Command {
    /// Register an user
    Register {
        /// Username
        #[arg(short, long)]
        username: String,
    },

    /// Login an already registered user
    Login {
        /// Username
        #[arg(short, long)]
        username: String,
    },
}
