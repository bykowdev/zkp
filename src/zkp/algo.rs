use num_bigint::{BigUint, RandBigInt};

#[derive(Debug, Default)]
pub struct ServerParameters {
    /// A large prime number.
    pub p: BigUint,
    /// A prime order subgroup size.
    pub q: BigUint,
    /// Generator for the subgroup.
    pub g: BigUint,
    /// Another generator.
    pub h: BigUint,
}

impl ServerParameters {
    // Taken from https://www.rfc-editor.org/rfc/rfc5114#section-2
    pub fn init() -> ServerParameters {
        // The hexadecimal value of the prime is:
        let p = BigUint::from_bytes_be(
            &hex::decode("B10B8F96A080E01DDE92DE5EAE5D54EC52C99FBCFB06A3C69A6A9DCA52D23B616073E28675A23D189838EF1E2EE652C013ECB4AEA906112324975C3CD49B83BFACCBDD7D90C4BD7098488E9C219A73724EFFD6FAE5644738FAA31A4FF55BCCC0A151AF5F0DC8B4BD45BF37DF365C1A65E68CFDA76D4DA708DF1FB2BC2E4A4371").unwrap(),
        );

        // The hexadecimal value of the generator is:
        let g = BigUint::from_bytes_be(
            &hex::decode("A4D1CBD5C3FD34126765A442EFB99905F8104DD258AC507FD6406CFF14266D31266FEA1E5C41564B777E690F5504F213160217B4B01B886A5E91547F9E2749F4D7FBD7D3B9A92EE1909D0D2263F80A76A6A24C087A091F531DBF0A0169B6A28AD662A4D18E73AFA32D779D5918D08BC8858F4DCEF97C2A24855E6EEB22B3B2E5").unwrap(),
        );

        // The generator generates a prime-order subgroup of size:
        let q = BigUint::from_bytes_be(
            &hex::decode("F518AA8781A8DF278ABA4E7D64B7CB9D49462353").unwrap(),
        );

        // Another generator:
        // h = g^i, where i == 428A9317C7D97799327E41, another random 26 digit prime
        let i = BigUint::from_bytes_be(&hex::decode("428A9317C7D97799327E41").unwrap());
        let h = g.modpow(&i, &p);

        ServerParameters { p, q, g, h }
    }

    pub fn compute_exp(&self, x: &BigUint) -> (BigUint, BigUint) {
        let r1 = self.g.modpow(x, &self.p);
        let r2 = self.h.modpow(x, &self.p);
        (r1, r2)
    }

    pub fn solve_challenge(&self, k: &BigUint, c: &BigUint, x: &BigUint) -> BigUint {
        match *k >= c * x {
            true => (k - c * x).modpow(&BigUint::from(1u32), &self.q), // just an easy use of modpow to actually do mod.,
            false => &self.q - (c * x - k).modpow(&BigUint::from(1u32), &self.q),
        }
    }

    pub fn verify(
        &self,
        r1: &BigUint,
        r2: &BigUint,
        y1: &BigUint,
        y2: &BigUint,
        c: &BigUint,
        s: &BigUint,
    ) -> bool {
        let a = *r1
            == (&self.g.modpow(s, &self.p) * y1.modpow(c, &self.p))
                .modpow(&BigUint::from(1u32), &self.p);

        let b = *r2
            == (&self.h.modpow(s, &self.p) * y2.modpow(c, &self.p))
                .modpow(&BigUint::from(1u32), &self.p);

        a && b
    }
}

pub fn random_biguint(ceil: &BigUint) -> BigUint {
    let mut rng = rand::thread_rng();
    rng.gen_biguint_below(ceil)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_algo_hardcoded() {
        let server_parameters = ServerParameters {
            p: BigUint::from(67u32),
            g: BigUint::from(4u32),  // generator
            q: BigUint::from(33u32), // g^q mod p = 1, 4^33 mod 67 = 1
            h: BigUint::from(64u32), // h = g^i, where i == 3
        };

        let x = BigUint::from(5u32);
        let k = BigUint::from(8u32);

        let (y1, y2) = server_parameters.compute_exp(&x);
        assert_eq!(y1, BigUint::from(19u32));
        assert_eq!(y2, BigUint::from(25u32));

        let (r1, r2) = server_parameters.compute_exp(&k);
        assert_eq!(r1, BigUint::from(10u32));
        assert_eq!(r2, BigUint::from(62u32));

        let c = BigUint::from(4u32);
        let s = server_parameters.solve_challenge(&k, &c, &x);
        assert_eq!(s, BigUint::from(21u32));

        let verified = server_parameters.verify(&r1, &r2, &y1, &y2, &c, &s);
        assert!(verified);
    }

    #[test]
    fn test_algo_random() {
        let server_parameters = ServerParameters {
            p: BigUint::from(67u32),
            g: BigUint::from(4u32),  // generator
            q: BigUint::from(33u32), // g^q mod p = 1, 4^33 mod 67 = 1
            h: BigUint::from(64u32), // h = g^i, where i == 3
        };

        let x = BigUint::from(20u32);
        let k = random_biguint(&server_parameters.q);

        let (y1, y2) = server_parameters.compute_exp(&x);
        assert_eq!(y1, BigUint::from(6u32));
        assert_eq!(y2, BigUint::from(15u32));

        let (r1, r2) = server_parameters.compute_exp(&k);

        let c = random_biguint(&server_parameters.q);
        let s = server_parameters.solve_challenge(&k, &c, &x);

        let verified = server_parameters.verify(&r1, &r2, &y1, &y2, &c, &s);
        assert!(verified);
    }
}
