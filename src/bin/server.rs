use std::collections::BTreeMap;
use std::env;
use std::error::Error;
use std::sync::Arc;

use env_logger::Env;
use num_bigint::BigUint;
use tokio::sync::Mutex;
use tonic::{transport::Server, Code, Request, Response, Status};
use uuid::Uuid;

use zkp::zkp::algo::random_biguint;
use zkp::zkp::algo::ServerParameters;
use zkp::zkp::zkp_auth::auth_server::AuthServer;
use zkp::zkp::zkp_auth::{
    auth_server::Auth, AuthenticationAnswerRequest, AuthenticationAnswerResponse,
    AuthenticationChallengeRequest, AuthenticationChallengeResponse, RegisterRequest,
    RegisterResponse,
};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let port: String = match env::var_os("ZKP_SERVER_PORT") {
        Some(p) => p.into_string().unwrap(),
        None => "10067".to_string(), // default parameter
    };

    log::info!("Starting ZKP auth server");
    log::info!("Port: {}", port);

    let server_db: ServerDB = ServerDB {
        server_parameters: ServerParameters::init(),
        auth_process: Arc::new(Mutex::new(BTreeMap::new())),
        users_parameters: Arc::new(Mutex::new(BTreeMap::new())),
    };

    Server::builder()
        .add_service(AuthServer::new(server_db))
        .serve(
            format!("0.0.0.0:{}", port)
                .parse()
                .expect("Could not parse port"),
        )
        .await?;

    Ok(())
}

// Should be changed for SQL DB
#[derive(Debug, Default)]
pub struct ServerDB {
    pub server_parameters: ServerParameters,
    pub auth_process: Arc<Mutex<BTreeMap<String, (String, BigUint)>>>, // auth_id, (username, c)
    pub users_parameters: Arc<Mutex<BTreeMap<String, UserParameters>>>, // username, user y1/y2/r1/r2
}

#[derive(Debug, Default)]
pub struct UserParameters {
    pub y1: BigUint,
    pub y2: BigUint,
    pub r1: BigUint,
    pub r2: BigUint,
}

#[tonic::async_trait]
impl Auth for ServerDB {
    async fn register(
        &self,
        request: Request<RegisterRequest>,
    ) -> Result<Response<RegisterResponse>, Status> {
        let req = request.into_inner();

        log::info!("Registering user: {:?}", &req.user);

        self.users_parameters.lock().await.insert(
            req.user.clone(),
            UserParameters {
                y1: BigUint::from_bytes_be(&req.y1),
                y2: BigUint::from_bytes_be(&req.y2),
                ..Default::default()
            },
        );

        log::info!("Successfully registered user: {:?}", req.user);
        Ok(Response::new(RegisterResponse {}))
    }

    async fn create_authentication_challenge(
        &self,
        request: Request<AuthenticationChallengeRequest>,
    ) -> Result<Response<AuthenticationChallengeResponse>, Status> {
        let req = request.into_inner();
        log::info!("Creating challenge for user: {:?}", req.user);

        match self.users_parameters.lock().await.get_mut(&req.user) {
            Some(user_parameters) => {
                user_parameters.r1 = BigUint::from_bytes_be(&req.r1);
                user_parameters.r2 = BigUint::from_bytes_be(&req.r2);

                let auth_id: String = Uuid::new_v4().to_string();
                let c: BigUint = random_biguint(&self.server_parameters.q);

                self.auth_process
                    .lock()
                    .await
                    .insert(auth_id.clone(), (req.user, c.clone()));

                Ok(Response::new(AuthenticationChallengeResponse {
                    auth_id,
                    // This is ugly, but types are correct
                    c: c.to_bytes_be(),
                }))
            }
            None => {
                log::warn!("User: {:?} not found or invalid", &req.user);
                Err(Status::new(
                    Code::NotFound,
                    format!("User: {:?} not found or invalid", &req.user),
                ))
            }
        }
    }

    async fn verify_authentication(
        &self,
        request: Request<AuthenticationAnswerRequest>,
    ) -> Result<Response<AuthenticationAnswerResponse>, Status> {
        let req = request.into_inner();
        let binding = self.users_parameters.lock().await;

        log::info!("Verifying authentication for auth id: {:?}", &req.auth_id);

        match self.auth_process.lock().await.get(&req.auth_id) {
            Some((username, c)) => {
                let user_parameters = binding.get(username).unwrap();

                match self.server_parameters.verify(
                    &user_parameters.r1,
                    &user_parameters.r2,
                    &user_parameters.y1,
                    &user_parameters.y2,
                    c,
                    &BigUint::from_bytes_be(&req.s),
                ) {
                    true => {
                        let session_token = Uuid::new_v4();
                        log::info!(
                            "Authentication complete for user: {:?}. Creating session token: {:?}",
                            username,
                            session_token
                        );
                        Ok(Response::new(AuthenticationAnswerResponse {
                            session_id: session_token.to_string(),
                        }))
                    }
                    false => {
                        log::warn!("AuthID: {:?} wrong solution", &req.auth_id);
                        Err(Status::new(
                            Code::PermissionDenied,
                            format!("AuthID: {:?} wrong solution", &req.auth_id),
                        ))
                    }
                }
            }
            None => {
                log::warn!("AuthID: {:?} not found or invalid", &req.auth_id);
                Err(Status::new(
                    Code::NotFound,
                    format!("AuthID: {:?} not found or invalid", &req.auth_id),
                ))
            }
        }
    }
}
