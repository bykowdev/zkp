use std::{env, thread, time};

use clap::Parser;
use env_logger::Env;
use names::Generator;
use num_bigint::BigUint;
use passwords::PasswordGenerator;
use tonic::transport::Channel;

use zkp::zkp::{
    algo::{random_biguint, ServerParameters},
    cli::*,
    zkp_auth::{
        auth_client::AuthClient, AuthenticationAnswerRequest, AuthenticationChallengeRequest,
        RegisterRequest,
    },
};

#[tokio::main]
async fn main() {
    let cli = Cli::parse();
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let addr: String = match env::var_os("ZKP_SERVER_ADDR") {
        Some(p) => p.into_string().unwrap(),
        None => "127.0.0.1".to_string(), // default parameter
    };

    let port: String = match env::var_os("ZKP_SERVER_PORT") {
        Some(p) => p.into_string().unwrap(),
        None => "10067".to_string(), // default parameter
    };

    log::info!("Starting ZKP auth client");
    log::info!("Attempting to connect to: http://{}:{}", addr, port);

    let mut client: Client = Client::new(format!("http://{}:{}", addr, port)).await;

    match &cli.command {
        Some(Command::Register { username }) => {
            let password = rpassword::prompt_password("Provide password: ").unwrap();
            client.register(username.to_string(), password).await;
        }
        Some(Command::Login { username }) => {
            let password = rpassword::prompt_password("Provide password: ").unwrap();
            client.login(username.to_string(), password).await;
        }
        None => {
            log::warn!("No command provided, assuming demo mode, non interactive");

            loop {
                let mut generator = Generator::default();
                let username = generator.next().unwrap();

                let pg = PasswordGenerator {
                    length: 20,
                    numbers: true,
                    lowercase_letters: true,
                    uppercase_letters: true,
                    symbols: true,
                    spaces: false,
                    exclude_similar_characters: false,
                    strict: true,
                };
                let password = pg.generate_one().unwrap();

                // Leaking for demonstration
                log::info!("Username: {}", username);
                log::info!("Password: {}", password);

                client.register(username.clone(), password.clone()).await;
                client.login(username, password).await;

                log::info!("Sleeping 30s before next run");
                thread::sleep(time::Duration::from_secs(30));
            }
        }
    }
}

#[derive(Debug)]
pub struct Client {
    pub server_parameters: ServerParameters,
    pub inner: AuthClient<Channel>,
}

impl Client {
    async fn new(address: String) -> Client {
        let connexion = AuthClient::connect(address).await;
        match connexion {
            Ok(c) => {
                log::info!("Client connected");

                // This could be a request to the server for it's public information (g,h,p)
                Client {
                    server_parameters: ServerParameters::init(),
                    inner: c,
                }
            }
            Err(e) => {
                log::error!("Could not connect to zkp auth server with error: {:?}", e);
                panic!(); // Client should not run if it can't connect to server
            }
        }
    }

    async fn register(&mut self, username: String, password: String) {
        let (y1, y2) = self
            .server_parameters
            .compute_exp(&BigUint::from_bytes_be(password.as_bytes()));

        let registration_request = RegisterRequest {
            user: username,
            y1: y1.to_bytes_be(),
            y2: y2.to_bytes_be(),
        };

        match self.inner.register(registration_request).await {
            Ok(_) => log::info!("Registration successful."),
            Err(e) => log::error!("Registration failed with error: {:?}", e),
        }
    }

    async fn login(&mut self, username: String, password: String) {
        let k = random_biguint(&self.server_parameters.q);
        let (r1, r2) = self.server_parameters.compute_exp(&k);

        let challenge_request = AuthenticationChallengeRequest {
            user: username,
            r1: r1.to_bytes_be(),
            r2: r2.to_bytes_be(),
        };

        match self
            .inner
            .create_authentication_challenge(challenge_request)
            .await
        {
            Err(e) => log::error!("Auth challenge failed with error: {:?}", e),
            Ok(challenge_response) => {
                let challenge_response = challenge_response.into_inner();
                let c = BigUint::from_bytes_be(&challenge_response.c);
                let s = self.server_parameters.solve_challenge(
                    &k,
                    &c,
                    &BigUint::from_bytes_be(password.as_bytes()),
                );

                let verification_request = AuthenticationAnswerRequest {
                    auth_id: challenge_response.auth_id,
                    s: s.to_bytes_be(),
                };

                match self.inner.verify_authentication(verification_request).await {
                    Ok(verification_response) => {
                        log::info!(
                            "Login successful. Session token: {}",
                            verification_response.into_inner().session_id
                        )
                    }
                    Err(e) => log::error!("Login failed with error: {:?}", e),
                }
            }
        }
    }
}
