#! /bin/bash

podman build --file docker/Dockerfile_client --tag zkp_client .
podman build --file docker/Dockerfile_server --tag zkp_server .
