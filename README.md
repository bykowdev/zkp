# ZKP Authentication Demonstration

This repo shows a simple example of a ZKP Authentiaction implementation. Using the Chaum-Pedersen protocol.

## Usage

By default, without specific arguments, the code will run in "demo" mode, which makes the client loop and perform a new registration and login every 30s.

#### Cargo local:
```
cargo run --bin server
cargo run --bin client
```

#### Docker local:
```
./build.sh
```

#### Docker compose from Gitlab registry:
Images are built within my k8s cluster using gitlab-runner.

```
podman-compose up
```

#### K8s:
Made at home on own server, deployment files can be found [here](https://gitlab.com/bykowdev/damocles/-/blob/main/kubernetes/apps/development/zkp/app/helmrelease.yaml?ref_type=heads). Message me for more details

This is using GitOps principle of deployement.

## Environment variables
- `ZKP_SERVER_ADDR` server address, default: `127.0.0.1` for client to reach local server
- `ZKP_SERVER_PORT` server port, default `10067`

## Further considerations and improvements
Some things that could be done to further improve this project

- sql DB to store users, rather than in memory
- REST API for client, to actually use the client other than CLI
- Reforge build/deploy to include running the tests in pipeline
